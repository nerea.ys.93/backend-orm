const express = require("express");

const app = express();
const Sequelize = require("Sequelize");
const sequelize = new Sequelize("prueba", "root", "1234", {
  host: "localhost",
  port: "3307",
  dialect: "mysql",
});

sequelize
  .authenticate()
  .then(() => {
    console.log("Conectado");
  })
  .catch((err) => {
    console.log("No se conecto");
  });

const empresa = sequelize.define(
  "empresa",
  {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
    },
    nombre: Sequelize.STRING,
    cuit: Sequelize.STRING,
  },
  {
    freezeTableName: true,
  }
);

// let empresasDB

app.get("/traer/empresas", (request, response) => {
  empresa
    .findAll({ attributes: ["id", "nombre", "cuit"] })
    .then((emp) => {
      response.json(emp);
    })
    .catch((err) => {
      response.send(err);
    });
});

const sucursales = sequelize.define("sucursales", {
  id: { type: Sequelize.INTEGER, primaryKey: true },
  nombre: Sequelize.STRING,
  direccion: Sequelize.STRING,
});

//let sucursalDB

app.get("/traer/sucursales",(request, response) => {
    sucursales
    .findAll({ attributes: ["id", "nombre", "direccion"] })
    .then((emp) => {
        response.json(emp);
    })
    .catch((err) => {
        response.send(err);
    });
});

app.listen(3000, () => {
  console.log("El servidor esta escuachando en el puerto 3000");
});
